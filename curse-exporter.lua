#!/usr/bin/env lua5.3
local here = arg[0]:match("^(.-/?)[^/]-$")
if here == "" then
	here = "./"
end
package.path = here.."?.lua;"..package.path
-- Get json and stdlua from bitbucket.org/deltanedas/mmc-utils
-- Install lua-http - https://github.com/daurnimator/lua-http
-- Put stdlua.lua and json.lua in same path as this, you can run it anywhere now

-- sudo apt install liblua5.3-dev
-- sudo luarocks install luasec
local STDLua = require("stdlua")
local JSON = require("json")

local AccountsData = {}
local Config = {
	Data = {}
}

local Instance = {
	Data = {},
	Name = "",
	Path = "",
	Minecraft = ""
}

local Args = table.new{...}

if #Args == 0 then
	printf("Run %s with a MultiMC instance as an argument.\n", STDLua.GetName())
	os.exit(22)
end

local OS = STDLua.OS
local Home = STDLua.Home

local Dirs = {
	Cache = Home..".cache/curse-exporter/", -- mcmod.info's of unzipped mods are stored here
	MultiMC = "",
	Instances = "",
	Accounts = ""
}

if OS == "Windows" then
	Dirs.MultiMC = Home.."multimc/"
elseif OS == "Linux" or OS == "UNIX" then
	Dirs.MultiMC = Home..".local/share/multimc/"
else
	-- Unknown OS
	io.write("Please enter your MultiMC path: ")
	Dirs.MultiMC = io.read("*l")

	if not Dirs.MultiMC:match("/$") then
		Dirs.MultiMC = Dirs.MultiMC.."/"
	end
	Dirs.MultiMC = Dirs.MultiMC:gsub("^~/?", Home)
end

Dirs.Instances = Dirs.MultiMC.."instances/"
Dirs.Accounts = Dirs.MultiMC.."accounts.json"
-- inb4 token stealer reeeee

local function LoadData()
	Instance.Name = Args:concat(" "):escape("Bash")

	if Instance.Name:sub(1, 1) == "/" then
		Instance.Path = Instance.Name
	else
		Instance.Path = Dirs.Instances..Instance.Name
	end

	if Instance.Path:sub(-1) ~= "/" then
		Instance.Path = Instance.Path.."/"
	end

	if not STDLua.Exists(Instance.Path:sub(1,-2)) then
		printf("Instance path '%s' does not exist.\n", Instance.Path)
		os.exit(2)
	end

	if not STDLua.IsDir(Instance.Path) then
		printf("Instance path '%s' is not a directory.\n", Instance.Path)
		os.exit(20)
	end

	printf("Using instance %s.\n", Instance.Name)
	Instance.Minecraft = Instance.Path..".minecraft/"

	if not STDLua.Exists(Instance.Minecraft) then
		if STDLua.Exists(Instance.Path.."minecraft/") then -- Imported packs use "minecraft" for some reason...
			Instance.Minecraft = Instance.Path.."minecraft/"
		else
			printf("No .minecraft directory found in '%s', creating one.\n", Instance.Path)
			STDLua.MakeDir(Minecraft)
		end
	end

	if not STDLua.IsDir(Instance.Minecraft) then
		printf("'%s' is a file.\n", Instance.Minecraft)
		os.exit(20)
	end
	-- Load minecraft and modloader version stuff
	local Text, Error = STDLua.Read(Instance.Path.."mmc-pack.json")
	if not Text then
		print("E: "..Error)
		os.exit(1)
	end

	Instance.Data = JSON.decode(Text)
	if not Instance.Data then
		print("E: Failed to parse instance json!")
		os.exit(1)
	end

	-- Load instance name, settings
	local Text, Error = STDLua.Read(Instance.Path.."instance.cfg")
	if not Text then
		print("E: "..Error)
		os.exit(1)
	end

	for Line, Pair in pairs(Text:split("\n")) do
		local Key = Pair:match("^(.-)=")
		local Value = Pair:match("=(.-)$")
		if Key and Value then
			Config.Data[Key] = Value
		else
			printf("Failed to parse line %d of instance.cfg!\n", Line)
		end
	end

	-- Load usernames
	local Text, Error = STDLua.Read(Dirs.Accounts)
	if not Text then
		print("E: "..Error)
		os.exit(1)
	end

	AccountsData = JSON.decode(Text)
	if not AccountsData then
		print("E: Failed to parse accounts json!")
		os.exit(1)
	end
end

local BackupNames = { -- No estimates needed, already know em
	["just-enough-items"] = "jei",
	["malilib-fabric"] = "malilib",
	["foamfix"] = "foamfix-for-minecraft",
	["grimoire-of-gaia-3"] = "grimoire-of-gaia",
	["atomicstrykers-battletowers"] = "atomicstrykers-battle-towers",
	["minecraft-comes-alive"] = "minecraft-comes-alive-mca",
	["mobamputation"] = "mob-amputation",
	["mobdismemberment"] = "mob-dismemberment",
	["ruins-spawning-system"] = "ruins-structure-spawning-system", -- Change your mods id cunt i cant stand it; "ruins" isnt even taken ffs y not
	["the-camping-mod-2"] = "the-camping-mod",
	["mrcrayfishs-furniture-mod"] = "mrcrayfish-furniture-mod",
	["iron-chest"] = "iron-chests"
}

local BackupFormats = {
	{"%-", "-"}, -- Try url as-is but encoded
	{"%-", ""},
	{"%-", "_"},
	{"^(.+)$", "the-%1"},
	{"^(.+)$", "%1-mod"},
	{"^(.+)$", "the-%1-mod"}
}

local function CharToHex(c)
	return string.format("%%%02X", string.byte(c))
end

local function Encode(url)
	if type(url) == "string" then
		url = url:gsub("\n", "\r\n")
		url = url:gsub("([^%w _%%%-%.~])", CharToHex)
		url = url:gsub(" ", "+")
		return url
	end
end

local function GetBackupURL(ModName, Version)
	local Base = "https://curseforge.com/minecraft/mc-mods/"
	ModName = Encode(ModName:lower():gsub(" ", "-"):gsub("'", "")):gsub("%%%x%x", "-") -- Remove symbols
	ModName = BackupNames[ModName] or ModName
	if ModName == "waila" and Version:match("^1%.1[234]%.%d$") then
		ModName = "hwyla"
	end
	local Started = false

	for _, Format in ipairs(BackupFormats) do
		Old, Format = Format[1], Format[2]
		local Modified = ModName:gsub(Old, Format)
		if Modified ~= ModName or not Started then
			Started = true
			local New = Base..Modified
			local Data, Error = STDLua.Download(New, nil, true, nil, 10)

			if not Data then
				print("Failed to download data from curseforge: "..Error)
				os.exit(1)
			end

			if Data:match('<asp:content id="errorContent" contentplaceholderid="MainContent" runat="server">%s*<h2>Error</h2>%s*Sorry, an error occurred while processing your request%.%s*</asp:content>') then
				print("Url '%s' caused an ISE for curseforge.")
			end

			if Error == "200" then
				return Data:match('<meta property="og:url" content="(https://www%.curseforge%.com/minecraft/mc%-mods/.-)" />')  or New -- Redirects are silent
			end
			if Error ~= "404" then
				printf("Attempted mod URL '%s' returned code %s.\n", New, Error)
				os.exit(1)
			end
		end
	end
	io.write("What is "..ModName.."'s curseforge mod id? (minecraft/mc-mods/...) ")
	local input = io.read("*l")
	return Base..input
end

local function GetModURL(ModName, Version) -- DuckDuckGo for mods ID as amazonforge(TM)s search is dogshit
	local Data, Error = STDLua.Download("https://duckduckgo.com/lite/?q=site%3Acurseforge.com%2Fminecraft%2Fmc-mods%2F+"..ModName:escape("GET"), nil, true)
	if not Data then
		if Error:match("Connection timed out%.$") then -- Banned
			print("Your IP is banned from duckduckgo. :(")
			Data = "If this error persists, please let us know: error-lite@duckduckgo.com"
		else
			print("Failed to search for mod id: "..Error)
			return Data, Error
		end
	end
	Data = Data:gsub("%%3A", ":"):gsub("%%2F", "/"):gsub("%%2D", "-")
	local URL = Data:match('<a rel="nofollow" href=.-(https://www%.curseforge%.com/minecraft/mc%-mods/.-)["/]')
	if not URL then
		if Data == "If this error persists, please let us know: error-lite@duckduckgo.com" then -- Suspended
			return GetBackupURL(ModName, Version)
		end
		return nil, "Failed to find URL."
	end
	return URL
end

local Filters = { -- I dont know how they are made
	["1.14.4"] = 7469,
	["1.14.3"] = 7413,
	["1.14.2"] = 7361,
	["1.14.1"] = 7344,
	["1.14"] = 7318,
	["1.13.2"] = 7132,
	["1.13.1"] = 7107,
	["1.13"] = 7081,
	["1.12.2"] = 6756,
	["1.12.1"] = 6711,
	["1.12"] = 6580,
	["1.11.2"] = 6452,
	["1.11.1"] = 6451,
	["1.11"] = 6317,
	["1.10.2"] = 6170,
	["1.10"] = 6144,
	["1.9.4"] = 6084,
	["1.9"] = 5946,
	["1.8.9"] = 5806,
	["1.8.8"] = 5703,
	["1.8"] = 4455,
	["1.7.10"] = 4449,
	["1.7.2"] = 361,
	["1.6.4"] = 326,
	["1.6.2"] = 320,
	-- End of my support, update your mods please
}

local function GetBackupData(ModName, Version)
	local Members = {}
	local Versions = {}
	local ID, Title

	local Filter = Filters[Version] -- Minecraft:NUMBER

	if Filter then
		Filter = "?filter-game-version=2020709689%3A"..Filter
	else
		return nil, "No version found for "..Version.."."
	end

	local Text, Error = STDLua.Download("https://curseforge.com/minecraft/mc-mods/"..ModName.."/files/all"..Filter)
	if not Text then
		return Text, Error
	end

	if Error ~= "200" then
		return nil, Error
	end

	Title = Text:match("<title> Files %- (.-) %- Mods.-</title>")
	ID = Text:match("<span>Project ID</span>%s*<span>(%d-)</span>")

	for Name, Title in Text:gmatch("<a href=\"/members/.-\">%s*<span>%s*(.-)%s*</span>%s*</a>%s*</p>%s*<p class=\"text%-xs\">(.-)</p>") do
		table.insert(Members, {
			username = Name,
			title = Title
		})
	end

	for ID, Name in Text:gmatch('<a data%-action="file%-link" href="/minecraft/mc%-mods/.-/files/(.-)">(.-)</a>') do
		table.insert(Versions, {
			id = ID,
			name = Name
		})
	end

	return {
		members = Members,
		versions = {[Version] = Versions},
		id = ID,
		title = Title
	}
end

local function GetModData(ModID, Version)
	local Text, Error = STDLua.Download("https://api.cfwidget.com/mc-mods/minecraft/"..ModID)
	if not Text then
		print(Error)
		os.exit(1)
	end

	local Data = JSON.decode(Text)
	if Data.error then
		if Data.error == "in_queue" then
			local Timeout = Data.message:match("Please retry your request in (%d+) seconds.")
			print("Waiting for "..Timeout.." seconds.")
			os.execute("sleep "..Timeout) -- Wait 10 seconds
			return GetModData(ModID, Version) -- Retry
		elseif Data.message == "Project cannot be found in CurseForge database." then
			-- This is needed because cfwidget 404s a lot for existing mods.
			return GetBackupData(ModID, Version) -- Parse HTML of files/all
		else
			return nil, Data.message
		end
	end

	for Name, _ in pairs(Data.versions) do
		if not (Name == Version or Name == Version:match("^(.-)%.%d$")) then
			Data.versions[Name] = nil -- Remove unused versions
		end
	end

	return Data
end

local function GetUsername()
	local Email = assert(AccountsData.activeAccount)
	for _, Account in pairs(assert(AccountsData.accounts)) do
		if Account.username == Email then -- Get username of active account only
			local ProfileID = Account.activeProfile
			for _, Profile in pairs(assert(Account.profiles)) do
				if Profile.id == ProfileID then
					return Profile.name or "USERNAME" -- Put in manifest.json
				end
			end
		end
	end
end

local function GetVersion()
	if Instance.Data.formatVersion ~= 1 then
		return nil, nil, "Unsupported mmc-pack format."
	end

	if type(Instance.Data.components) ~= "table" or #(Instance.Data.components) == 0 then
		printf("Malformed JSON, \"components\" empty or not an array.\n")
		os.exit(-1)
	end

	local Vanilla, Modloader
	for i,v in pairs(Instance.Data.components) do
		if v.cachedName == "Minecraft" then
			Vanilla = v.version
		elseif v.cachedName == "Forge" then
			Modloader = "forge-"..v.version
			printf("Found Forge version %s.\n", v.version)
		elseif v.cachedName == "Fabric Loader" then -- Keep in mind curse packs dont yet support fabric I think
			Modloader = "fabric-"..v.version
			printf("Found Fabric version %s.\n", v.version)
		end
	end

	return Vanilla, Modloader
end

local LinkFormat = "\t<li><a href=\"https://minecraft.curseforge.com/mc-mods/%s\">%s (by %s)</a></li>"
local ModlistFormat = [[
<ul>
%s
</ul>]]

local FileFormat = [[
		{
			"projectID": %s,
			"fileID": %s,
			"required": true
		}]]
local ManifestFormat = [[
{
	"minecraft": {
		"version": "%s",
		"modLoaders": [
			{
				"id": "%s",
				"primary": true
			}
		]
	},
	"manifestType": "minecraftModpack",
	"manifestVersion": 1,
	"name": "%s",
	"version": "1.0",
	"author": "%s",
	"overrides": "overrides",
	"files": [
%s
	]
}]]

STDLua.MakeDir(Dirs.Cache)

local function GetModName(Mod, Escaped)
	local Text, _, _, Code = STDLua.Execute("unzip -p \"%s\" mcmod.info 2>/dev/null", Escaped) -- 2>/ pipes stderr to a file
	if Text == "" then
		if Code == 11 then -- Its a fabric mod
			Text, _, _, Code = STDLua.Execute("unzip -p \"%s\" fabric.mod.json 2>/dev/null", Escaped)
			if Text == "" then
				if Code == 11 then
					if Mod:lower():match("^optifine") then -- Everyone uses optifine, youll need to check with other "no-redistribution" mods yourself
						print("* \033]31mOptiFine is proprietary software and as such the end user must install it themself.\033]0m")
						return
					elseif Mod:lower():match("^betterfps") then
						return "BetterFPS"
					else
						printf("Couldn't find mcmod.info or fabric.mod.json in %s, adding to overrides.\n", Mod)
						return nil, Mod
					end
				else
					printf("Failed to extract fabric.mod.json from %s: %d.\n", Mod, Code)
					os.exit(1)
				end
			else
				Info = JSON.decode(Text)
				if not Info then
					printf("Malformed JSON in fabric.mod.json of %s!\n", Mod)
					os.exit(2)
				end
				if Info.schemaVersion == 1 or type(Info.name) == "string" then
					return Info.name
				else
					printf("%s's fabric.mod.json could not be interpreted!\n", Mod)
					os.exit(3)
				end
			end
		else
			printf("Failed to extract mcmod.info from %s: %d.\n", Mod, Code)
			os.exit(1)
		end
	else
		Info = JSON.decode(Text)
		if not Info then
			printf("Malformed JSON in mcmod.info of %s!\n", Mod)
			os.exit(2)
		end
		if (Info.modListVersion or 1) == 1 then
			return (Info[1] or {}).name
		elseif Info.modListVersion == 2 then
			return ((Info.modList or {})[1] or {}).name
		else
			printf("%s's mcmod.info could not be interpeted, skipping!\n", Mod)
		end
	end
end

local function CreateLists()
	local ModsPath = Instance.Minecraft.."mods/"
	local Links = table.new()
	local Files = table.new()
	local Overrides = {}

	local Vanilla, ModLoader = GetVersion()

	for _, Mod in ipairs(STDLua.List(ModsPath)) do
		local ModPath = ModsPath..Mod
		if STDLua.IsDir(ModPath) then
			printf("\t'%s' is a directory, skipping...\n", Mod)
		elseif not Mod:match("^.+%.jar$") then
			printf("\t'%s' is not a mod, skipping...\n", Mod)
		else
			local Skipping = false

			local Info = {}
			local Escaped = ModPath:escape("Bash")
			local CachedInfo = Dirs.Cache..Mod:gsub("%.jar$", ".json")

			if not Skipping then
				local Data
				print("Found "..Mod..".")
				if not STDLua.Exists(CachedInfo) then
					local Name, Override = GetModName(Mod, Escaped)
					if Name then
						local ModURL, Error = GetModURL(Name, Vanilla)
						if ModURL then
							ModID = ModURL:match("^.+/(.-)$") -- Last file is the mod id
							Data, Error = GetModData(ModID, Vanilla)
							if Data then
								for _, Member in pairs(Data.members) do
									if Member.title == "Owner" then
										ModOwner = Member.username
										break
									end
								end
								local Success, Error = STDLua.Write(CachedInfo, JSON.encode{
									name = ModID,
									title = Data.title,
									id = Data.id,
									owner = ModOwner
								})
								if not Success then
									print(Error)
									os.exit(2)
								end
							else
								printf("Failed to get mod data for %s: %s\n", Mod, Error)
								os.exit(1)
							end
						else
							printf("Failed to look up mod id of %s: %s\n", Mod, Error)
							os.exit(1)
						end
					elseif Override then
						Overrides.mods = Overrides.mods or {}
						table.insert(Overrides.mods, Mod)
						Skipping = true
					else
						printf("Failed to get mod name from %s.\n", Mod)
						Skipping = true
					end
				end

				if not Skipping then
					local Text, Error = STDLua.Read(CachedInfo)
					local Info = JSON.decode(Text)
					local Data, Error = GetModData(Info.name, Vanilla)
					if not Data then
						printf("Failed to get mod data for %s: %s\n", Mod, Error)
						os.exit(1)
					end

					local Aborted = false
					local ModFiles = Data.versions[Vanilla] or Data.versions[Vanilla:match("^(.-)%.%d$")]
					if ModFiles then
						local Forge = ModLoader:match("^forge") ~= nil
						for _, File in ipairs(ModFiles) do
							local Loader, Current
							LatestFile = File.id

							if Forge and File.name:lower():match("fabric") then -- 1.14 mods for forge and fabric wew lad
								Current = "Forge"
								Loader = "Current"
							elseif not Forge and File.name:lower():match("forge") then
								Current = "Fabric"
								Loader = "Forge"
							else -- No modloader conflict
								break
							end
							if Current then
								if STDLua.PullYesNo("%s is likely for %s but you have %s installed, do you want to keep it anyways? Y/N: ", File.name, Loader, Current) then
									break
								end
							end
						end
					end

					if LatestFile then
						Links:insert(LinkFormat:format(Info.id, Info.title, Info.owner))
						Files:insert(FileFormat:format(Info.id, LatestFile))
					end
				end
			end
		end
	end

	return ModlistFormat:format(Links:concat("\n")),
		ManifestFormat:format(Vanilla, ModLoader, Config.Data.name, GetUsername() or "USERNAME", Files:concat(",\n")), Overrides
end

LoadData()

local Mods = Instance.Minecraft.."mods/"
if not STDLua.Exists(Mods) then
	print("Mods directory not found.")
	os.exit(1)
end

if not STDLua.IsDir(Mods) then
	printf("'%s' is a file.\n", Mods)
	os.exit(20)
end

print("Your username is "..assert(GetUsername().."."))

local Modlist, Manifest, Overrides = CreateLists()

local Pack = Config.Data.name.." Generated Pack"
if not STDLua.Exists(Pack) then
	if not STDLua.MakeDir(Pack.."/overrides/") then
		print("Failed to create pack output directory!")
		os.exit(1)
	end
end
io.write("Saving modlist.html... ")
local Success, Error = STDLua.Write(Pack.."/modlist.html", Modlist)
if not Success then
	print("Error: "..Error.."\n")
	os.exit(1)
end
print("Done!")

io.write("Saving manifest.json... ")
local Success, Error = STDLua.Write(Pack.."/manifest.json", Manifest)
if not Success then
	print("Error: "..Error.."\n")
	os.exit(1)
end
print("Done!")

if table.count(Overrides) > 0 then
	print("Copying overrides, please make sure you do not violate the licenses of their copyright holders.")
	for Dir, List in pairs(Overrides) do
		io.write("Copying overrides for "..Dir.."... ")
		local Path = Pack.."/overrides/"..Dir.."/"
		if not STDLua.Exists(Path) then
			if not STDLua.MakeDir(Path) then
				print("Failed to create overrides/"..Dir.."/!")
				os.exit(1)
			end
		end

		for _, Override in pairs(List) do
			STDLua.Copy(Instance.Minecraft..Dir.."/"..Override, Path..Override)
		end
		print("Done!")
	end
end