﻿local request = require("http.request")
-- Requires lua http - https://github.com/daurnimator/lua-http

local STDLua = {}
STDLua.Version = "1.5.1"

local function Exists(Path)
	Path = tostring(Path)
	local File = io.open(Path, "r")
	if File then
		File:close()
	end
	return File ~= nil
end

local function IsDir(Path)
	Path = tostring(Path)
	if not Exists(Path) then
		return nil, "Directory does not exist."
	end
	local File, Error = io.open(Path, "r")
	if not File then
		return nil, Error
	end
	local Text, Error, Code = File:read()
	File:close()
	if Text ~= nil then
		return false
	end
	if Code ~= 21 then
		return nil, Error
	end
	return true
end

local function Read(Path)
	Path = tostring(Path)
	if not Exists(Path) then
		return nil, 2
	end
	if IsDir(Path) then
		return nil, 21
	end
	local File, Error = io.open(Path, "r")
	if File then
		local Data = File:read("a")
		File:close()
		return Data
	end
	return nil, Error
end

local function Write(Path, Data)
	Path = tostring(Path)
	Data = tostring(Data)
	local File, Error = io.open(Path, "w")
	if not File then
		return nil, Error
	end
	File:write(Data)
	File:close()
	return true
end

local function Append(Path, Data)
	Path = tostring(Path)
	Data = tostring(Data)
	local File, Error = io.open(Path, "a")
	if not File then
		return nil, Error
	end
	File:write(Data)
	File:close()
	return true
end

local function Execute(Command, ...) -- Escape input yourself
	-- Like command %s -> command escape(%s)
	if type(Command) ~= "string" then
		return nil, "Bad argument #1 (Command) to 'Execute' (string expected, got "..type(Command)..")"
	end
	if os.execute() == 0 then
		return nil, "Shell not found!"
	end
	Command = Command:format(...)
	local File = io.popen(Command, "r")
	local Output = File:read("a")
	return Output or "", File:close()
end

local function List(Path)
	if not Exists(Path) then
		return nil, 2
	end
	if not IsDir(Path) then
		return nil, 20
	end
	Path = Path:escape()
	local Files = ""
	if STDLua.OS == "Windows" then
		Files = Execute("dir /b /d "..Path)
	else
		Files = Execute("ls -A '"..Path.."'")
	end
	Files = Files:split("\n")
	return Files
end

local function MakeDir(Path)
	if type(Path) ~= "string" then
		return nil, "bad argument #1 (Path) to MakeDir (string expected, got "..type(Path)..")"
	end
	if Exists(Path) then
		return nil, "File or directory exists."
	end
	return os.execute("mkdir -p '"..Path.."'")
end

local function Download(Url, Path, Spoof, Body)
	if type(Url) ~= "string" then
		return nil, "Bad argument #1 (Url) to 'Download' (string expected, got "..type(Url)..")"
	end

	if Url:sub(1,5) == "http:" then
		Url = "https:"..Url:sub(6)
	end
	local Request = request.new_from_uri(Url)

	if Body then
		Request.headers:upsert(":method", "POST")
		Request:set_body(Body)
	end
	if Spoof then
		Request.headers:upsert("user-agent", "Palememe/5.0 (Gentoo Linux; WOW64; rv:19.0) gento/20160101 Palememe/40.0") -- Set useragent to bullshit
	end

	local Headers, Stream = Request:go(5)
	if not Headers then
		return nil, "Error while downloading file: "..tostring(Stream).."."
	end

	local Body = Stream:get_body_as_string()

	if Path == nil then
		return Body
	end
	return Write(Path, Body)
end

local function Copy(Path, Destination)
	if not Exists(Path) then
		return nil, 2
	end
	if not Exists(Destination) then
		return nil, 2
	end

	Path = Path:escape("BashLite")
	Destination = Path:escape("BashLite")
	if OS == "Windows" then
		Execute("xcopy '%s' '%s' /O /X /E /H /K", Path, Destination)
	else
		Execute("cp -rf '%s' '%s'", Path, Destination)
	end
	return true
end

-- OS stuff

local OS = "Windows"

if package.config:sub(1, 1) == "/" then
	OS = "Unix"
	if Execute("uname") == "Linux\n" then
		OS = "Linux"
	elseif Execute("uname") == "Darwin\n" then
		OS = "MacOS"
	end
end

local Home
if OS == "Windows" then
	Home = os.getenv("APPDATA"):gsub("\\", "/").."/"
else
	Home = os.getenv("HOME").."/"
end

-- Global functions

local function Split(String, Seperator)
	local Table = {}
	Seperator = (tostring(Seperator) or " "):escape("Patterns")
	String = tostring(String)
	if #String > 0 then
		if Seperator == "" then
			for i = 1, #String do
				table.insert(Table, String:sub(i, i))
			end
		else
			for Text in string.gmatch(String, "[^"..Seperator.."]+") do
				table.insert(Table, Text)
			end
		end
	end
	return Table
end

local function Merge(T1, T2)
	local Return = {}
	for i,v in pairs(T1 or {}) do
		Return[i] = v
	end
	for i,v in pairs(T2 or {}) do
		Return[i] = v
	end
	local M1 = getmetatable(T1)
	local M2 = getmetatable(T2)
	if M1 or M2 then
		setmetatable(Return, Merge(M1, M2))
	end
	return Return
end

local function Reverse(Table)
	local Return = {}
	for i, v in ipairs(Table) do
		Return[v] = i
	end
	return Return
end

local function ToBoolean(value)
	if value == "true" then
		return true
	elseif value == "false" then
		return false
	end
end

local EscapeTypes = {
	Bash = {
		["&"] = "\\&",
		[";"] = "\\;",
		["\""] = "\\\"",
		["'"] = "\\'",
		["%.%."] = "\\..",
		[">"] = "\\>",
		["<"] = "\\<",
		["%$%("] = "\\$\\(",
		["%)"] = "\\)",
		["%?"] = "\\%?",
		["|"] = "\\|",
		["{"] = "\\{",
		["}"] = "\\}",
		["`"] = "\\`",
		["~"] = Home:sub(1, -2)
	},
	BashLite = {
		["\""] = "\\\"",
		["'"] = "\\'",
		["`"] = "\\`"
	},
	GET = {
		["%s+"] = "%%20",
		["!"] = "%%21",
		["\""] = "%22",
		["#"] = "%%23",
		["%$"] = "%%24",
		["%%"] = "%%25",
		["&"] = "%%26",
		["'"] = "%%27",
		["%("] = "%%28",
		["%)"] = "%%29",
		["%*"] = "%%2A",
		["%+"] = "%%2B",
		[","] = "%%2C",
		["%-"] = "%%2D",
		["%."] = "%%2E",
		["/"] = "%%2F",
		["0"] = "%%30",
		["1"] = "%%31",
		["2"] = "%%32",
		["3"] = "%%33",
		["4"] = "%%34",
		["5"] = "%%35",
		["6"] = "%%36",
		["7"] = "%%37",
		["8"] = "%%38",
		["9"] = "%%39",
		[":"] = "%%3A",
		[";"] = "%%3B",
		["<"] = "%%3C",
		["="] = "%%3D",
		[">"] = "%%3E",
		["?"] = "%%3F",
		["@"] = "%%40"
	},
	Patterns = {
		["%("] = "%%(",
		["%)"] = "%%)",
		["%."] = "%%.",
		["%+"] = "%%+",
		["%-"] = "%%-",
		["%*"] = "%%*",
		["%["] = "%%[",
		["%]"] = "%%]",
		["%^"] = "%%^",
		["%$"] = "%%$",
		["%?"] = "%%?",

		["%%a"] = "%%%%a",
		["%%c"] = "%%%%c",
		["%%d"] = "%%%%d",
		["%%g"] = "%%%%g",
		["%%l"] = "%%%%l",
		["%%p"] = "%%%%p",
		["%%s"] = "%%%%s",
		["%%u"] = "%%%%u",
		["%%w"] = "%%%%w",
		["%%x"] = "%%%%x",

		["%%A"] = "%%%%A",
		["%%C"] = "%%%%C",
		["%%D"] = "%%%%D",
		["%%G"] = "%%%%G",
		["%%L"] = "%%%%L",
		["%%P"] = "%%%%P",
		["%%S"] = "%%%%S",
		["%%U"] = "%%%%U",
		["%%W"] = "%%%%W",
		["%%X"] = "%%%%X"
	},
	Markdown = {
		["%*"] = "\\*",
		["%+"] = "\\+",
		["%-"] = "\\-",
		["%("] = "\\(",
		["%)"] = "\\)",
		["%["] = "\\[",
		["%]"] = "\\]",
		["%-"] = "\\-",
		["%."] = "\\.",
		["`"] = "\\`",
		["_"] = "\\_",
		["~"] = "\\~",
		["#"] = "\\#",
		["{"] = "\\{",
		["}"] = "\\}",
		["!"] = "\\!",
		["|"] = "\\|",
		["> "] = "\\> "
	},
	Flip = {
		a = "ɐ",
		b = "q",
		c = "ɔ",
		d = "p",
		e = "ǝ",
		f = "ɟ",
		g = "ƃ",
		h = "ɥ",
		i = "ᴉ",
		j = "ɾ",
		k = "ʞ",
		l = "ן",
		m = "ɯ",
		n = "u",
		p = "d",
		q = "b",
		r = "ɹ",
		t = "ʇ",
		u = "n",
		v = "ʌ",
		w = "ʍ",
		y = "ʎ",

		A = "∀",
		B = "𐐒",
		C = "Ɔ",
		D = "Ɑ",
		E = "Ǝ",
		F = "Ⅎ",
		G = "⅁",
		J = "ſ",
		K = "ꓘ",
		L = "⅂",
		M = "W",
		P = "Ԁ",
		Q = "Ό",
		R = "ᴚ",
		T = "⊥",
		U = "∩",
		V = "Λ",
		W = "M",
		Y = "⅄",

		["1"] = "⇂",
		["2"] = "ᘔ",
		["3"] = "Ɛ", -- No fucking idea lmao
		["4"] = "߈",
		["5"] = "ϛ",
		["6"] = "9",
		["7"] = "ㄥ",
		["9"] = "6",

		["!"] =  "ᴉ",
		["\""] = "„",
		-- No £
		["^"] = "⌄",
		["&"] = "⅋",
		-- No *

		["_"] = "‾", -- Arabs lol amirite
		[";"] = "؛",
		[","] = "'",
		["'"] = ",",
		["."] = "˙",
		["?"] = "¿"
	},
	Ebin = {-- Very ebin :DDD i recommend xDD
		["america"] = "clapistan",
		["meme"] = "maymay",
		["some"] = "sum",
		["epic"] = "ebin",
		["kek"] = "geg",
		["right"] = "rite",

		["ng"] = "nk",
		["ic"] = "ig",

		["ing"] = "ign",
		["alk"] = "olk",

		["ys"] = "yz",
		["ws"] = "wz",
		["us"] = "uz",
		["ts"] = "tz",
		["ss"] = "sz",
		["rs"] = "rz",
		["ns"] = "nz",
		["ms"] = "mz",
		["ls"] = "lz",
		["is"] = "iz",
		["gs"] = "gz",
		["fs"] = "fz",
		["es"] = "es",
		["ds"] = "dz",
		["bs"] = "bz",

		["tr"] = "dr",
		["ts"] = "dz",
		["pr"] = "br",
		["nt"] = "dn",
		["mm"] = "m",
		["lt"] = "ld",
		["kn"] = "gn",
		["cr"] = "gr",
		["ck"] = "gg",

		["va"] = "ba",
		["up"] = "ub",
		["pi"] = "bi",
		["pe"] = "be",
		["po"] = "bo",
		["ot"] = "od",
		["op"] = "ob",
		["nt"] = "nd",
		["ke"] = "ge",
		["it"] = "id",
		["iv"] = "ib",
		["et"] = "ed",
		["ex"] = "egz",
		["ev"] = "eb",
		["co"] = "go",
		["ck"] = "gg",
		["ca"] = "ga",
		["ap"] = "ab",
		["af"] = "ab",
		["ux"] = "ugz", -- Its gunnoo/LINUGGZZ :DDDD
		["in"] = "en",

		["th"] = "d",
		["wh"] = "w"
	}
}

local function Escape(Text, Type, EscapeBackslashes, EscapePercents, Lazy)
	if type(Text) == "string" then
		if EscapeTypes[Type] == nil then
			Type = "Bash"
		end
		EscapeBackslashes = EscapeBackslashes or true
		EscapePercents = EscapePercents or Type == "Patterns" or false
		if Text:find("\\") and EscapeBackslashes then
			Text = Text:gsub("\\", "\\\\")
		end
		if Text:find("%%") and EscapePercents then
			Text = Text:gsub("%%", "%%%%")
		end

		local Ret = ""
		if Lazy then
			Ret = Text
			for Pattern, Replacement in opairs(EscapeTypes[Type]) do
				Ret = Ret:gsub(Pattern, Replacement)
			end
		else
			Text = Text:encode() -- Turn it into an array of UTF-8 characters
			for _, Part in pairs(Text) do
				for Pattern, Replacement in opairs(EscapeTypes[Type]) do
					if Part:match(Pattern) then
						Part = Part:gsub(Pattern, Replacement)
						break
					end
				end
				Ret = Ret..Part
			end
		end
		return Ret
	end
end

local function Count(Table)
	local count = 0
	for i,v in pairs(Table) do
		count = count + 1
	end
	return count
end

local function __genOrderedIndex( t )
	local orderedIndex = {}
	for key in pairs(t) do
		if type(key) == "string" then
			table.insert(orderedIndex, key)
		end
	end
	table.sort(orderedIndex)
	return orderedIndex
end

local function OrderedNext(t, state)
	local key = nil
	if state == nil then
		t.__orderedIndex = __genOrderedIndex( t )
		key = t.__orderedIndex[1]
	else
		for i = 1, Count(t.__orderedIndex) do
			if t.__orderedIndex[i] == state then
				key = t.__orderedIndex[i+1]
			end
		end
	end
	if key then
		return key, t[key]
	end
	t.__orderedIndex = nil
	return
end

function OrderedPairs(Table)
	return OrderedNext, Table
end

local function Crop(Table, Length)
	if type(Length) ~= "number" then
		return Table
	end
	local Return = {}
	local TableLength = table.count(Table)
	if Length == 0 or (Length % TableLength) == 0 then
		return Table
	elseif Length < 0 then
		for Iterator = 1 + TableLength + ((TableLength - Length) % TableLength * -1), TableLength do
			table.insert(Return, Table[Iterator])
		end
	else
		for Iterator = 0, TableLength + ((TableLength + Length) % TableLength * -1) do
			table.insert(Return, Table[Iterator])
		end
	end
	return Return
end

local function InternalConcat(Table, Depth, Tables)
	Depth = Depth or 0
	if Depth == 0 then
		return "{\n"..InternalConcat(Table, 1).."\n}"
	else
		Tables = Tables or {}
		local StringSoFar = ""
		local Indentation = string.rep("	", Depth)
		for i, v in pairs((type(Table) == "table" and Table) or  {}) do
			StringSoFar = StringSoFar..Indentation
			if type(i) == "string" then
				StringSoFar = StringSoFar.."\""..i.."\" = "
			elseif type(i) == "nil" then
				StringSoFar = StringSoFar.."nil = "
			else
				StringSoFar = StringSoFar..tostring(i).." = "
			end
			if type(v) == "string" then
				StringSoFar = StringSoFar.."\""..v.."\""
			elseif type(v) == "nil" then
				StringSoFar = StringSoFar.."nil"
			elseif type(v) == "table" and not Tables[v] then
				Tables[v] = true
				StringSoFar = StringSoFar.."{\n"..InternalConcat(v, Depth + 1, Tables).."\n"..Indentation.."}"
			else
				StringSoFar = StringSoFar..tostring(v)
			end
			StringSoFar = StringSoFar..",\n"
		end

		if StringSoFar:sub(-2) == ",\n" then
			StringSoFar = StringSoFar:sub(1, -3)
		end
		return StringSoFar
	end
end

local function RecursiveConcat(Table)
	return InternalConcat(Table)
end

local function PrintF(Text, ...)
	return io.write(Text:format(...))
end

local function GetName()
	local Path = debug.getinfo(2, "S").source:sub(2)
	return Path:match("^.+/(.+)$") or Path
end

local function NewTable(t)
	if type(t) ~= "table" then
		t = {}
	end
	t = t or {}
	setmetatable(t, {__index = table})
	for i, v in pairs(t) do
		if type(v) == "table" then
			t[i] = NewTable(v)
		end
	end
	return t
end

local function TCopy(T)
	local Ret = {}
	for i, v in pairs(T) do
		Ret[i] = v
	end
	setmetatable(Ret, getmetatable(T) or {})
	return Ret
end

local function TCompare(Value, ...) -- OR between Value and everything after it
	local T = {...}
	if #T > 0 and type(Value) ~= "nil" then
		for _, Comparison in pairs(T) do
			if Comparison == Value then
				return true
			end
		end
		return false
	end
end

local function SCompare(Value, ...) -- OR between Value and match of everything after it
	local T = {...}
	if #T > 0 and type(Value) ~= "nil" then
		for _, Comparison in pairs(T) do
			if Value:match(Comparison) then
				return true
			end
		end
		return false
	end
end

local function PullYesNo(...) -- Do you want to continue Y/N
	printf(...)
	local input = io.read("*l"):lower()
	if input:compare("^y$", "^yes", "^yeah*", "yep", "absolutely", "affirmative", "positively", "positive") then
		return true
	elseif input:compare("^n$", "^no", "^nope$", "^nah+", "abort", "cancel", "quit") then
		return false
	end
	return PullYesNo(...)
end

local function Timestamp(Place) -- Defaults to Seconds and Nanoseconds
	Place = tonumber(Place or 3)
	if not Place then
		return nil, "Bad argument #1 (Place) to 'Timestamp' (number expected, got "..type(Place)..")"
	end

	Place = math.floor(Place)
	if Place > 9 or Place < 1 then
		return os.time()
	end

	local Output = Execute("date +%%s%%"..Place.."N")
	if not Output then
		return nil, "Date is not supported on this machine."
	end

	return tonumber(Output:sub(Place))
end

local function Encode(String)
	local Ret = table.new()
	for Char in String:gmatch(utf8.charpattern) do
		Ret:insert(Char)
	end
	return Ret
end

local function TFlip(T) -- Only works on arrays, works from left to centre
	local i, j = 1, #T

	while i < j do
		T[i], T[j] = T[j], T[i]
		i = i + 1
		j = j - 1
	end

	return T
end

local function Round(Number, Place)
	Place = Place or 0
	return math.floor(Number * 10^Place + 0.5) / 10^Place
end

-- Functions
STDLua.Exists = Exists
STDLua.IsDir = IsDir
STDLua.Read = Read
STDLua.Write = Write
STDLua.Append = Append
STDLua.Execute = Execute
STDLua.List = List
STDLua.MakeDir = MakeDir
STDLua.Download = Download
STDLua.GetName = GetName
STDLua.Copy = Copy
STDLua.EscapeTypes = EscapeTypes
STDLua.PullYesNo = PullYesNo
STDLua.Timestamp = Timestamp

-- Variables
STDLua.OS = OS
STDLua.Home = Home

-- Global Functions
-- Say its namespace pollution but these are reallllyyyy useful
string.split = Split
string.escape = Escape
string.encode = Encode
string.compare = SCompare
table.merge = Merge
table.reverse = Reverse
table.count = Count
table.crop = Crop
table.rconcat = RecursiveConcat
table.copy = TCopy
table.compare = TCompare
table.new = NewTable
table.flip = TFlip
math.round = Round
toboolean = ToBoolean
opairs = OrderedPairs
printf = PrintF

EscapeTypes.FlipInverse = {}
for i, v in opairs(EscapeTypes.Flip) do
	EscapeTypes.Flip[i] = nil
	EscapeTypes.Flip[i:escape("Patterns")] = v
	EscapeTypes.FlipInverse[v:escape("Patterns")] = i
end

-- Fix compat with old scripts
pack = table.pack
unpack = table.unpack

return STDLua
