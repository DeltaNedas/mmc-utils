# mmc-utils
This is a repository of some Lua scripts I've written to improve MultiMC.
GNU/Linux only for the most part.

# Credit
Uses [json.lua](https://github.com/rxi/json.lua) licensed under the MIT License.
See [LICENSE](LICENSE).
