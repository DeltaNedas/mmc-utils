#!/usr/bin/env lua5.3
local here = arg[0]:match("^(.-/?)[^/]-$")
if here == "" then
	here = "./"
end
package.path = here.."?.lua;"..package.path
-- Get json and stdlua from bitbucket.org/deltanedas/mmc-utils
-- Install lua-http - https://github.com/daurnimator/lua-http
-- Put stdlua.lua and json.lua in same path as this, you can run it anywhere now

local STDLua = require("stdlua")
local JSON = require("json")

local Dirs = {
	MultiMC = "",
	Mods = "",
	Instances = "",
	ResourcePacks = "",
	Options = ""
}

local OS = STDLua.OS
local Home = STDLua.Home

if OS == "Windows" then
	Dirs.MultiMC = Home.."multimc/"
elseif OS == "Linux" or OS == "UNIX" then
	Dirs.MultiMC = Home..".local/share/multimc/"
else
	-- Unknown OS
	io.write("Please enter your MultiMC path: ")
	Dirs.MultiMC = io.read("*l")

	if not Dirs.MultiMC:match("/$") then
		Dirs.MultiMC = Dirs.MultiMC.."/"
	end
	Dirs.MultiMC = Dirs.MultiMC:gsub("^~/?", Home)
end

Dirs.Mods = Dirs.MultiMC.."mods/"
Dirs.Instances = Dirs.MultiMC.."instances/"
Dirs.ResourcePacks = Dirs.MultiMC.."resourcepacks/"
Dirs.Options = Dirs.MultiMC.."options/" -- Keep vanilla options in sync

local Args = table.new{...}

if #Args == 0 then
	printf("Run %s with a MultiMC instance as an argument.\n", arg[0])
	os.exit(22)
end

local InstanceName = Args:concat(" "):escape("Bash")
local InstancePath = ""

if InstanceName:sub(1, 1) == "/" then
	InstancePath = InstanceName
else
	InstancePath = Dirs.Instances..InstanceName
end

if InstancePath:sub(-1) ~= "/" then
	InstancePath = InstancePath.."/"
end

if not STDLua.Exists(InstancePath:sub(1,-2)) then
	printf("Instance path '%s' does not exist.\n", InstancePath)
	os.exit(2)
end

if not STDLua.IsDir(InstancePath) then
	printf("Instance path '%s' is not a directory.\n", InstancePath)
	os.exit(20)
end

printf("Using instance %s.\n", InstanceName)

local Minecraft = InstancePath
if STDLua.Exists(Minecraft..".minecraft/") then
	Minecraft = Minecraft..".minecraft/"
elseif STDLua.Exists(Minecraft.."minecraft/") then
	Minecraft = Minecraft.."minecraft/"
else
	printf("No minecraft folder found in '%s'.\n", InstancePath)
	Minecraft = Minecraft..".minecraft"
	STDLua.MakeDir(Minecraft)
end

if not STDLua.IsDir(Minecraft) then
	printf("'%s' is a file.\n", Minecraft)
	os.exit(20)
end

local Text, Error = STDLua.Read(InstancePath.."mmc-pack.json")
if not Text then
	printf("Could not read %smmc-pack.json: %s.\n", InstancePath, tostring(Error))
	os.exit(-1)
end

local Data, Error = table.new(JSON.decode(Text))
if not Data then
	printf("Failed to parse mmc-pack.json: %s.\n", tostring(Error))
	os.exit(-1)
end

if Data.linkOptions ~= false then
	if type(Data.components) ~= "table" or #Data.components == 0 then
		printf("Malformed JSON, \"components\" not an array or empty.\n")
		os.exit(-1)
	end

	local Version
	local Vanilla = true
	for i,v in pairs(Data.components) do
		if v.cachedName == "Minecraft" then
			Version = v.version
		elseif v.cachedName == "Forge" or v.cachedName == "Fabric Loader" then
			printf("Found %s, not linking options.\n", v.cachedName)
			Vanilla = false
		end
	end

	if Version and Vanilla then
		printf("Linking options for MC %s.\n", Version)
		local OptionsExt = "options_"..Version:escape()..".txt"
		local LinkedOptions = Dirs.Options..OptionsExt
		local OptifineExt = "optionsof_"..Version:escape()..".txt"
		local LinkedOptifine = Dirs.Options..OptifineExt

		if (not STDLua.Exists(LinkedOptions)) and STDLua.Exists(Minecraft.."options.txt") then
			print("Copying over options.txt as it don't exist already.")
			STDLua.Copy(Minecraft.."options.txt", LinkedOptions)
		end
		STDLua.Execute("ln -sf '%s' '%soptions.txt'", LinkedOptions, Minecraft)
		if (not STDLua.Exists(LinkedOptifine)) and STDLua.Exists(Minecraft.."optionsof.txt") then
			print("Copying over optionsof.txt as it don't exist already.")
			STDLua.Copy(Minecraft.."optionsof.txt", LinkedOptifine)
		end
		STDLua.Execute("ln -sf '%s' '%soptionsof.txt'", LinkedOptifine, Minecraft)
	end
end

if Data.linkMods ~= false then
	local Mods = Minecraft.."mods/"

	if not STDLua.Exists(Mods) then
		return
	end

	if not STDLua.IsDir(Mods) then
		printf("'%s' is a file.\n", Mods)
		os.exit(20)
	end

	printf("Linking %d mods for %s.\n", #STDLua.List(Mods), InstanceName)
	for _, Mod in pairs(STDLua.List(Mods)) do
		local ModPath = Mods..Mod
		if not STDLua.Exists(Dirs.Mods..Mod) then
			printf("\t'%s' not found in '%s', skipping...\n", Mod, Dirs.Mods)
		elseif STDLua.IsDir(Dirs.Mods..Mod) then
			printf("\t'%s' is a directory, skipping...\n", Mod)
		else
			ModPath = ModPath:escape("BashLite")
			Mod = Mod:escape("BashLite")
			STDLua.Execute("ln -sf '%s' '%s'", Dirs.Mods..Mod, ModPath)
		end
	end
end
